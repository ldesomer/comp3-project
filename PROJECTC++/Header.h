//
//  Header.h
//  PROJECTC++
//
//  Created by Luc Desomer on 09/10/2017.
//  Copyright © 2017 Luc Desomer. All rights reserved.
//

#ifndef Header_h
#define Header_h
#define ways 2                  // number of ways
#define numberset 64            // number of sets 

FILE *fileaddress,*filedram,*filecache,*filenewdram=NULL;

unsigned long cpuaddr;          //address in the txt file
unsigned short setno;           // set number
unsigned short upperaddr;       // upper address
unsigned short word;            // word
unsigned short datatocopy;      // data to copy for the write function
unsigned short datatowrite;     // data to write for the write function
unsigned long  address;         // same than cpu address
unsigned short realcpu;         //address for a write instruction
int hits[ways];
int nmiss[ways];

int hitcountway0;               // number of hits on way0
int hitcountway1;               // number of hits on way1
int misscountway0;              // number of misses on way0
int misscountway1;              // number of misses on way1
int misscount;                  // number of total misses
int instruction;                // 0=read, 1= write

struct cacheline{ // structure of a cache line
    
    unsigned char tag;
    bool validflag; //is the line corrupted ?
    bool LRUflag;   // used for the replacement policy
    bool modified; // used for the write back policy
    char data[4]; // array of 4 bytes
    
};

void resetLRU(); // reset all the LRU of way0 with value 1 and of way1 with value 0, it means we will start to copy in way0
void resetall(); // reset the cache, use at the beginning of the main
void openfiles(); // open the files: CPU address, DRAM, cache
int checkLRU(unsigned short setno); // will return the way where the LRU of setno is 1
void resetarray(); // reset the arrays to count hits and missed
int totalmiss(); // count the misses
int read(unsigned short cpuaddr); // Read operation
int write(unsigned long cpuaddr); // Write operation

#endif /* Header_h */
