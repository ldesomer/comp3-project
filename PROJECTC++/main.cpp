//
//  main.cpp
//  PROJECTC++
//
//  Created by Luc Desomer on 28/09/2017.
//  Copyright © 2017 Luc Desomer. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include "Header.h"

cacheline cache[numberset][ways]; //creating cache


void resetLRU(){
    int i=0;
    int j=1;
    for(j=1; j<ways; j++){
    for(i=0; i<numberset; i++){
            cache[i][0].LRUflag=1;
            cache[i][j].LRUflag=0;
        }
    }
}

void resetall(){
    int i=0;
    int j=0;
    for(i=0; i<numberset; i++){
        for(j=0; j<ways; j++){
            cache[i][j].validflag=0;
            cache[i][j].modified=0;
            cache[i][j].tag=NULL;
        }
    }
}

void openfiles(){
    fileaddress=fopen("/Users/lucdesomer/Documents/Project/PROJECTC++/PROJECTC++/testaddress.txt","r");
    filedram=fopen("/Users/lucdesomer/Documents/Project/PROJECTC++/PROJECTC++/testdram.txt","r");
    filecache=fopen("/Users/lucdesomer/Documents/Project/PROJECTC++/PROJECTC++/testcache.txt","r+");
    filenewdram=fopen("/Users/lucdesomer/Documents/Project/PROJECTC++/PROJECTC++/testnewdram.txt","r+");
}

int checkLRU(unsigned short setno){
    int i=0;
    for (i=0; i<ways; i++){
        if(cache[setno][i].LRUflag==1 && cache[setno][i].validflag==0){ //first access, if we've never written something before
            return i;
        }
    }
    for (i=0; i<ways; i++){
        if (cache[setno][i].LRUflag==1){ //if something has already been written
            return i;
        }
    }
    
    return ways;
}

void ressetarray(){
    int i=0;
    for (i=0; i<ways; i++)
        hits[i]=0;
        nmiss[i]=0;
}

int totalmiss(){
    int i=0;
    int total=0;
    for (i=0; i<ways; i++){
        total=nmiss[i]+total;
    }
    return total;
}

int read(unsigned short cpuaddr){
    setno=(cpuaddr & 0x00ff)>>2;
    upperaddr=(cpuaddr & 0xff00)>>8;
    word=(cpuaddr & 0x0003);
    int i=0;
    int test=0;
    int j=0;
    int offset=0;
    int choix=0;
    for(i=0; i<ways; i++){
        
        
        if ((cache[setno][i].tag==upperaddr)&&(cache[setno][i].validflag==1)){ // if tag=upper address and if the valid tag=1
            
            if(cache[setno][i].modified==0){ //if modified=0, we just have to count a hit
            hits[i]++;
            printf("There's %d hit on way %d, setno: %hx, data: %c\n", hits[i], i, setno, cache[setno][i].data[word]);
            cache[setno][i].LRUflag=0;
            offset=i;
            test=offset;
            test++;
            while(test<ways){
                cache[setno][test].LRUflag=1;
                test++;
                
            }

            while(j<offset){
                cache[setno][j].LRUflag=1;
                j++;
            }
            choix=1;
            }
        }
        
        if(cache[setno][i].modified==1){ // if modified=1, we copy the data to dram before and we count a hit
        fprintf(filenewdram,"0x%hx%s\n", cpuaddr,cache[setno][i].data);
            hits[i]++;
            printf("There's %d hit on way %d, setno: %hx, data: %c\n", hits[i], i, setno, cache[setno][i].data[word]);
            cache[setno][i].LRUflag=0;
            cache[setno][i].modified=0;
            offset=i;
            test=offset;
            test++;
            while(test<ways){
                cache[setno][test].LRUflag=1;
                test++;
                
            }
            
            while(j<offset){
                cache[setno][j].LRUflag=1;
                j++;
            }
            choix=1;
        }
    
    }
    if(choix==0){ // there is a miss
       
            int n=0;
            n=checkLRU(setno);
            cache[setno][n].tag=upperaddr;
            cache[setno][n].validflag=1;
            
            while(fscanf(filedram, "%lx\n", &address) != EOF){
               unsigned short tutu;
                tutu=address>>16; // it is the cpu address
                if (cpuaddr==tutu){
                    datatocopy=address;
                    sprintf(cache[setno][n].data, "%hx", datatocopy); // copy the data regarding the number
                    if (datatocopy<=9){
                        cache[setno][n].data[3]=cache[setno][n].data[0];
                        cache[setno][n].data[0]='0';
                        cache[setno][n].data[1]='0';
                        cache[setno][n].data[2]='0';
                        
                    }
                    
                    else if (10<= datatocopy && datatocopy<=99){
                        cache[setno][n].data[2]=cache[setno][n].data[0];
                        cache[setno][n].data[3]=cache[setno][n].data[1];
                        cache[setno][n].data[1]='0';
                        cache[setno][n].data[0]='0';
                        
                    }
                    
                    else if (100<= datatocopy && datatocopy<=999){
                        cache[setno][n].data[3]=cache[setno][n].data[2];
                        cache[setno][n].data[2]=cache[setno][n].data[1];
                        cache[setno][n].data[1]=cache[setno][n].data[0];
                        cache[setno][n].data[0]='0';
                    }}}
        
            
            printf("Here is the data we copy on way %d :%s, setno :%hx\n", n,cache[setno][n].data, setno);
        fprintf(filecache,"Way %d: 0x%-2x,%c\n",n, setno,cache[setno][n].data[word]);

        offset=n;
            i=n;
            i++;
            while(i<ways){ //set the LRU to 1 on the other way
                cache[setno][i].LRUflag=1;
                i++;
            }
            while(j<offset){
                cache[setno][j].LRUflag=1;
                j++;
            }
            nmiss[n]++;
            cache[setno][n].LRUflag=0; // set the LRU to 0

            printf("There's  %d miss on way %d\n", nmiss[n], n);
            printf("There's %d miss in total\n", totalmiss());
            rewind(filedram);
    
    }

        return 0;

}

int write(unsigned long cpuaddr){
    setno=(cpuaddr & 0x00ff0000)>>18;
    upperaddr=(cpuaddr & 0xff000000)>>24;
    realcpu=(cpuaddr)>>16;
    word=(cpuaddr & 0x00030000);
    datatowrite=(cpuaddr & 0x0000ffff);
    int i=0;
    int test=0;
    int j=0;
    int offset=0;
    int choix=0;
    for(i=0; i<ways; i++){
        
        if ((cache[setno][i].tag==upperaddr)&&(cache[setno][i].validflag==1)&&(cache[setno][i].modified)==0){ //if the tag=upper address, valid=1 and the data has never been modified
            cache[setno][i].LRUflag=0;
            cache[setno][i].modified=1;
            sprintf(cache[setno][i].data, "%hx", datatowrite); // modify the data regarding the number
            if (datatowrite<=9){
                cache[setno][i].data[3]=cache[setno][i].data[0];
                cache[setno][i].data[0]='0';
                cache[setno][i].data[1]='0';
                cache[setno][i].data[2]='0';
                
            }
            
            else if (10<=datatowrite && datatowrite<=99){
                cache[setno][i].data[2]=cache[setno][i].data[0];
                cache[setno][i].data[3]=cache[setno][i].data[1];
                cache[setno][i].data[1]='0';
                cache[setno][i].data[0]='0';
                
            }
            
            else if (100<=datatowrite && datatowrite<=999){
                cache[setno][i].data[3]=cache[setno][i].data[2];
                cache[setno][i].data[2]=cache[setno][i].data[1];
                cache[setno][i].data[1]=cache[setno][i].data[0];
                cache[setno][i].data[0]='0';
            }
            fprintf(filecache,"Way %d: 0x%-2x,%c\n",i, setno,cache[setno][i].data[word]);
            printf("The following data has been written to the cache: %c, setno: 0x%-2x, way: %d \n", cache[setno][i].data[word], setno, i);
            offset=i;
            test=offset;
            test++;
            while(test<ways){ // update the LRU
                cache[setno][test].LRUflag=1;
                test++;
                
            }
            
            while(j<offset){
                cache[setno][j].LRUflag=1;
                j++;
            }
            choix=1;
            
        }
        
    
    
   else if ((cache[setno][i].tag==upperaddr)&&(cache[setno][i].validflag==1)&&(cache[setno][i].modified)==1){ //if tag=upper address, valid=1 and the data has already been modified

        fprintf(filenewdram,"0x%hx%s\n", realcpu,cache[setno][i].data); // copy in the new dram file
        cache[setno][i].LRUflag=0;
        cache[setno][i].modified=1;
        sprintf(cache[setno][i].data, "%hx", datatowrite); // update the data regarding the number
        if (datatowrite<=9){
            cache[setno][i].data[3]=cache[setno][i].data[0];
            cache[setno][i].data[0]='0';
            cache[setno][i].data[1]='0';
            cache[setno][i].data[2]='0';
            
        }
        
        else if (10<= datatowrite && datatowrite<=99){
            cache[setno][i].data[2]=cache[setno][i].data[0];
            cache[setno][i].data[3]=cache[setno][i].data[1];
            cache[setno][i].data[1]='0';
            cache[setno][i].data[0]='0';
            
        }
        
        else if (100<= datatowrite && datatowrite<=999){
            cache[setno][i].data[3]=cache[setno][i].data[2];
            cache[setno][i].data[2]=cache[setno][i].data[1];
            cache[setno][i].data[1]=cache[setno][i].data[0];
            cache[setno][i].data[0]='0';
        }
        fprintf(filecache,"Way %d: 0x%-2x,%c\n",i, setno,cache[setno][i].data[word]);
       printf("The following data has been written to the cache: %c, setno: 0x%-2x, way: %d \n", cache[setno][i].data[word], setno, i);
        offset=i;
        test=offset;
        test++;
        while(test<ways){ // update the LRU
            cache[setno][test].LRUflag=1;
            test++;
            
        }
        
        while(j<offset){
            cache[setno][j].LRUflag=1;
            j++;
        }
        choix=1;
        
    }

    }
    if(choix==0){ // if there is nothing in the cache regarding the upper address
        int n=0;
        n=checkLRU(setno);
        cache[setno][n].tag=upperaddr; // set the tag=upper address
        cache[setno][n].validflag=1;   // update the valid flag to 1
        cache[setno][n].modified=1;    // set modified to 1 because the data does not exist in the dram
        sprintf(cache[setno][n].data, "%hx", datatowrite); // copy the new data regarding the number
                if (datatowrite<=9){
                    cache[setno][n].data[3]=cache[setno][n].data[0];
                    cache[setno][n].data[0]='0';
                    cache[setno][n].data[1]='0';
                    cache[setno][n].data[2]='0';

                }
        
                else if (10<= datatowrite && datatowrite<=99){
                    cache[setno][n].data[2]=cache[setno][n].data[0];
                    cache[setno][n].data[3]=cache[setno][n].data[1];
                    cache[setno][n].data[1]='0';
                    cache[setno][n].data[0]='0';

                }
        
                else if (100<= datatowrite && datatowrite<=999){
                    cache[setno][n].data[3]=cache[setno][n].data[2];
                    cache[setno][n].data[2]=cache[setno][n].data[1];
                    cache[setno][n].data[1]=cache[setno][n].data[0];
                    cache[setno][n].data[0]='0';
                }
        
        
        fprintf(filecache,"Way %d: 0x%-2x,%c\n",n, setno,cache[setno][n].data[word]);
        printf("The following data has been written to the cache: %c, setno: 0x%-2x, way: %d \n", cache[setno][i].data[word], setno, i);

        offset=n;
        i=n;
        i++;
        while(i<ways){ // update the LRU
            cache[setno][i].LRUflag=1;
            i++;
        }
        while(j<offset){
            cache[setno][j].LRUflag=1;
            j++;
        }
        nmiss[n]++;
        cache[setno][n].LRUflag=0;
        rewind(filedram);
        
    }

    return 0;
}










int main(){
    printf("We are using a %d way set associative cache, with %d sets of %d blocks each (%d lines in total). \n\n\n", ways, numberset, ways, ways*numberset);
    resetall(); // reset the flag
    resetLRU(); // reset the LRU
    ressetarray(); // reset the arrays with hits and misses
    openfiles();  // open all the files

    while(fscanf(fileaddress, "%d %lx\n", &instruction, &cpuaddr) != EOF){ // loop through the test file
        if (instruction==0){
        read(cpuaddr);
        }
        if (instruction==1){
            write(cpuaddr);
            }
        }
        
        
        


    return 0;
}
